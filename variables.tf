variable "image" {
  description = "image name"
}

variable "tag" {
  description = "tag image"
  default = "latest"
}

variable "container_name" {
  description = "container name"
}

variable "env_variables" {
  description = "The container env variables"
  type = list
  default = []
}

variable "docker_host" {
  description = "docker host"
}

variable "ca_pem" {
  description = "ca.pem content file"
}

variable "cert_pem" {
  description = "cert.pem content file"
}

variable "key_pem" {
  description = "key.pem content file"
}
