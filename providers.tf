terraform {
  backend "http" {
  }
  required_providers {
    docker = {
      source = "terraform-providers/docker"
    }
    null = {
      source = "hashicorp/null"
    }
  }
  required_version = ">= 0.13"
}
# Configure the GitLab Provider
provider "docker" {
  host = var.docker_host

  ca_material   = var.ca_pem
  cert_material = var.cert_pem
  key_material  = var.key_pem
}
