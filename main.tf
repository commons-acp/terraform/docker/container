resource "docker_container" "container" {
  name  = var.container_name
  image = docker_image.image.latest
  rm = true
  env = var.env_variables
}

resource "docker_image" "image" {
  name = format("%s:%s",var.image,var.tag)
}